# **Using the Light Sensor HAT**


## **What you will need**

 ![Here is a light sensor HAT](https://gitlab.com/eee3088f_group-2/light-sensing-project/-/blob/main/3D_image_of_HAT.jpeg)

(1) A battery or micro USB cable for power supply 

(2) A USB micro cable to connect to a pc  

(3) STM32 microcontroller and the Light sensor hat  

(4) STM32CubeIDE 

 You can download the software [here](https://en.freedownloadmanager.org/Windows-PC/Serial-EEPROM-Programmer-FREE.html)

 

## **How to connect the hardware**  

(1) Mount the light sensor Hat on the stm32 microcntroller board 

(2) Connect the battery or USB port on the HAT to power the Hat

(3) Connect the STM32 to your pc via the USB cable


 

## **How to run the equivalent of a “hello-world” program**  
(1) Open STM32CubeIDE 

(2) Download the sample code 

(3) Review and run the sample code to begin getting readings

 

## **Licence: GNU GPL** 

If you wish to use the software in this document you must adhere to the [Software Licence](https://gitlab.com/eee3088f_group-2/light-sensing-project/-/blob/main/LICENSE)
