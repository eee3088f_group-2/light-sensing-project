Contributions are warm welcomed and anyone who would like to contribute to the light sensor project must follow 
the step-by-step guide process illustrated below. For all code and non-code related contributions or any errors 
spotted on the project must be brought up in the same process illustrated below, all programming languages accepted.  

 
To contribute: 

(a) Clone the EEE3088F Light Sensor GitLab Repository. 

(b) Make your changes. 

(c) When you’re done making changes, create a merge request ensuring that: 

        (i)   You provide enough information about the contibutions 

        (ii)  Your additions are new and necessary  

        (iii) You have reviewed your work and that it is error free 

(d) Assign the merge request to one of the project owners with full explaination of what exaclty is being contributed. 
